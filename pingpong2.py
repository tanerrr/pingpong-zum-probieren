"""
Ein Programm, das die Zahlen von 1 bis 100 ausgibt
Wenn eine Zahl durch 3 teilbar ist, gib stattdessen „Ping“ aus
Wenn eine Zahl durch 5 teilbar ist, gibt stattdessen „Pong“ aus
Wenn eine Zahl durch 3 und durch 5 teilbar ist, gibt „PingPong“ aus
"""

for k in range(1, 100):
    if k % 3 == 0 and k % 5 == 0:
        print("PingPong")
    elif k % 3 == 0:
        print("Ping")
    elif k % 5 == 0:
        print("Pong")
    else:
        print(k)
