"""
Ein Programm, das die Zahlen von 1 bis 100 ausgibt
Wenn eine Zahl durch 3 teilbar ist, gib stattdessen „Ping“ aus
Wenn eine Zahl durch 5 teilbar ist, gibt stattdessen „Pong“ aus
Wenn eine Zahl durch 3 und durch 5 teilbar ist, gibt „PingPong“ aus
"""

for zahl in range(1, 101):
    out = ""
    if zahl % 3 == 0:
        out += "Ping"
    if zahl % 5 == 0:
        out += "Pong"

    if "" != out:
        print(out)
    else:
        print(zahl)
