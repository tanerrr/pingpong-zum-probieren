"""
Ein Programm, das die Zahlen von 1 bis 100 ausgibt
Wenn eine Zahl durch 3 teilbar ist, gib stattdessen „Ping“ aus
Wenn eine Zahl durch 5 teilbar ist, gibt stattdessen „Pong“ aus
Wenn eine Zahl durch 3 und durch 5 teilbar ist, gibt „PingPong“ aus
"""


def ausgabe(zahl):
    """
    Diese Funktion gibt "Ping" zurück, wenn die Zahl durch 3 teilbar ist, "Pong" wenn sie durch 5 teilbar ist, zum Beispiel
    >>> ausgabe(1)
    1
    >>> ausgabe(3)
    'Ping'
    >>> ausgabe(5)
    'Pong'
    >>> ausgabe(15)
    'PingPong'
    >>> ausgabe(12)
    'Ping'
    """
    if zahl % 3 == 0 and zahl % 5 == 0:
        return "PingPong"
    elif zahl % 3 == 0:
        return "Ping"
    elif zahl % 5 == 0:
        return "Pong"
    else:
        return zahl


if __name__ == "__main__":
    import doctest
    doctest.testmod()
